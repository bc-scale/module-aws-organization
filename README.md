# Module Organization

Creates an a organization with several accounts.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_organizations_account.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_account) | resource |
| [aws_organizations_organization.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_organization) | resource |
| [aws_organizations_organizational_unit.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/organizations_organizational_unit) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_tags"></a> [account\_tags](#input\_account\_tags) | Map of tag to be applied on all accounts. | `map(string)` | `{}` | no |
| <a name="input_accounts"></a> [accounts](#input\_accounts) | Map of accounts to create. | <pre>map(object({<br>    email                      = string<br>    iam_user_access_to_billing = optional(string)<br>    parent_id                  = optional(string)<br>    role_name                  = optional(string)<br>    tags                       = optional(map(string))<br>  }))</pre> | `{}` | no |
| <a name="input_aws_service_access_principals"></a> [aws\_service\_access\_principals](#input\_aws\_service\_access\_principals) | List of AWS service principal names for which you want to enable integration with your organization. | `list(string)` | `null` | no |
| <a name="input_enabled_policy_types"></a> [enabled\_policy\_types](#input\_enabled\_policy\_types) | List of Organizations policy types to enable in the Organization Root. | `list(string)` | `null` | no |
| <a name="input_feature_set"></a> [feature\_set](#input\_feature\_set) | Specify "ALL" or "CONSOLIDATED\_BILLING". | `string` | `"ALL"` | no |
| <a name="input_organizational_unit_tags"></a> [organizational\_unit\_tags](#input\_organizational\_unit\_tags) | Map of tag to be applied on all OU's. | `map(string)` | `{}` | no |
| <a name="input_organizational_units"></a> [organizational\_units](#input\_organizational\_units) | Map of organizational units to create. | <pre>map(object({<br>    parent_id = optional(string)<br>    tags      = optional(map(string))<br>  }))</pre> | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_account_arns"></a> [account\_arns](#output\_account\_arns) | Map of accounts with their respective ARN. |
| <a name="output_account_ids"></a> [account\_ids](#output\_account\_ids) | Map of accounts with their respective ID. |
| <a name="output_arn"></a> [arn](#output\_arn) | ARN of the organization. |
| <a name="output_id"></a> [id](#output\_id) | Id of the organization. |
| <a name="output_organizational_unit_arns"></a> [organizational\_unit\_arns](#output\_organizational\_unit\_arns) | Map of OU's with their respective ARN. |
| <a name="output_organizational_unit_ids"></a> [organizational\_unit\_ids](#output\_organizational\_unit\_ids) | Map of OU's with their respective ID. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
