#####
# Organization
#####

variable "aws_service_access_principals" {
  description = "List of AWS service principal names for which you want to enable integration with your organization."
  type        = list(string)
  default     = null
}

variable "enabled_policy_types" {
  description = "List of Organizations policy types to enable in the Organization Root."
  type        = list(string)
  default     = null
}

variable "feature_set" {
  description = "Specify \"ALL\" or \"CONSOLIDATED_BILLING\"."
  type        = string
  default     = "ALL"

  validation {
    condition     = can(regex("^(ALL|CONSOLIDATED_BILLING)$", var.feature_set))
    error_message = "The feature_set value must be one of ALL or CONSOLIDATED_BILLING."
  }
}

#####
# Organizational Units
#####

variable "organizational_units" {
  description = "Map of organizational units to create."
  default     = {}
  type = map(object({
    parent_id = optional(string)
    tags      = optional(map(string))
  }))
}

variable "organizational_unit_tags" {
  description = "Map of tag to be applied on all OU's."
  type        = map(string)
  default     = {}
}

#####
# Accounts
#####

variable "accounts" {
  description = "Map of accounts to create."
  type = map(object({
    email                      = string
    iam_user_access_to_billing = optional(string)
    parent_id                  = optional(string)
    role_name                  = optional(string)
    tags                       = optional(map(string))
  }))
  default = {}
}

variable "account_tags" {
  description = "Map of tag to be applied on all accounts."
  type        = map(string)
  default     = {}
}
